<?php

namespace App\Console\Commands;

use App\Document;
use App\Token;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Output\ConsoleOutput;

class QueryCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:query {expression}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'The query command search for documents';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $output = new ConsoleOutput();

        $expression = $this->argument('expression');
        $is_valid =  preg_match('/^[A-Za-z0-9 \|\&\(\)]+$/', $expression);

        if (!$is_valid) {
            $output->writeln("Query error: The expression contains other special characters. Please use only |, &, (, ) as special characters.");
            return;
        }

        // TODO : validate if the expression is in the right format (conjunction, disjunction, parentheses)
        $expression_elements = explode(' ', $expression);
        foreach ($expression_elements as $key => $element) {
            // add query conditions
            if(ctype_alnum($element)) {
                $expression_elements[$key] = " name = '" . $element . "' ";
            }
            else {
                // replace |, & with OR, AND
                if ($element == '|') {
                    $expression_elements[$key] = " OR ";
                }
                if ($element == '&') {
                    $expression_elements[$key] = " AND ";
                }
            }
        }

        // create query conditions expression
        $query_conditions = implode(' ', $expression_elements);
        try {
            $documents = DB::select(DB::raw("select distinct d.id from documents d 
                                inner join document_tokens dt on d.id=dt.document_id 
                                inner join tokens t on dt.token_id = t.id
                                where $query_conditions"));

            if (!empty($documents)) {
                $ids = array_map(function ($ar) {return $ar->id;}, $documents);
                $output->writeln("Query results: " . implode(' ', $ids));
            }
            else {
                $output->writeln("Query error: no results found");
            }
        }
        catch (\Exception $e) {
            $output->writeln("Query error: " . $e->getMessage());
        }
    }
}

