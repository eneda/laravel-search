<?php

namespace App\Console\Commands;

use App\Document;
use App\Token;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Output\ConsoleOutput;

class IndexCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:index {document_id*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'The index command adds tokens to a document';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $output = new ConsoleOutput();
        $command_arguments = $this->argument('document_id');
        $document_id = $command_arguments[0];

        // check if document id is integer or not
        if (!ctype_digit($document_id)) {
            $output->writeln("Index error: Document id type is not valid. Please use an integer value.");
            return;
        }
        // check if tokens are added or not
        if (count($command_arguments) < 2) {
            $output->writeln("Index error: Please specify at least one token.");
            return;
        }

        $document = Document::find($document_id);
        if(!$document){
            //create a new document with specified id and default name
            $document = Document::create([
                'id' => $document_id,
                'title' => 'New document -' . $document_id,
            ]);
        }
        $detach = true;
        foreach ($command_arguments as $key => $value) {
            // ignore the first element (document id)
            if ($key == 0) {
                continue;
            }
            // display a message when tokens contain special characters
            if(!ctype_alnum($value)) {
                $output->writeln("Index error: '$value' token contains special characters. Token not saved");
                continue;
            }
            // if at least one token is valid, delete the relations with previous tokens
            if ($detach) {
                $document->tokens()->detach();
                $detach = false;
            }

            $token = Token::where('name', $value)->get()->first();
            if(!$token){
                //create a new token with specified name
                $token = Token::create([
                    'name' => $value,
                ]);
            }
            // create the relation between the document and token
            $document->tokens()->attach($token);
        }
        // if all the tokens are not valid (contain special characters), display a message
        if ($detach) {
            $output->writeln("Index error: No Token was saved for the document");
        }
        else {
            $output->writeln("Index ok: " . $document_id);
        }
    }
}

