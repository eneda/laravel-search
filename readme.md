### Requirements ###
- **PHP** >= 7.0.0

### Installation ###

- Install vendors:
```
composer install
```

- Copy .env.example and rename it .env:
```
cp .env.example .env
```

- Create database in MySQL
```
CREATE DATABASE YOUR_DB_NAME;
```

- Edit .env file with database name, username, password
```
DB_DATABASE=YOUR_DB_NAME
DB_USERNAME=YOUR_DB_USERNAME
DB_PASSWORD=YOUR_DB_PASSWORD
```

- Generate key:
```
php artisan key:generate
```

- Migrate DB:
```
php artisan migrate
```


##### Tests on Index and Query commands
Command is located in this path: <b>app/Console/Commands/IndexCommand.php</b>

Index commands :
```
php artisan command:index 1 soup tomato cream salt
php artisan command:index 2 cake sugar eggs flour sugar cocoa cream butter
php artisan command:index 1 bread butter salt
php artisan command:index 5 cake salt
```
Index commands with errors :
```
php artisan command:index 3 s.oup tomato cream salt
php artisan command:index ab s,.oup tomato cream salt
php artisan command:index 3
```

Command is located in this path: <b>app/Console/Commands/QueryCommand.php</b>
Query commands (please use double quotes) :
```
php artisan command:query "salt"
php artisan command:query "salt & cake"
php artisan command:query "(cake | egs) & cocoa"
```
Query commands with errors :
```
php artisan command:query "salt%$!"
php artisan command:query "carrot"
```